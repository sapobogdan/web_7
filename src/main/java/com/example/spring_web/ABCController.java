package com.example.spring_web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ABCController {

    @Autowired
    C c;

    @Autowired
    B b;

    @RequestMapping("/schimba")
    @ResponseBody
    public String schimba() {
        c.change();
        return "schimbat";
    }

    @RequestMapping("/arata")
    @ResponseBody
    public String arata() {
        return b.showId() + "";
    }
}
