package com.example.spring_web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class PersonController {

    @Autowired
    PersonService personService;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    LoginHolder loginHolder;

    @RequestMapping("/users")
    public ModelAndView showAllUsers(@RequestParam(value = "page", required = false) Integer pageIndex) {

        if(loginHolder.getCurrentUserId() == 0) {
            return new ModelAndView("redirect:/index.html");
        }
        if(pageIndex == null) {
            pageIndex = 1;
        }

        List<Person> nextPage = personService.getPersonPaginated(pageIndex + 1);
        boolean nextPageVisible = true;
        if(nextPage.size() == 0) {
            nextPageVisible = false;
        }
        ModelAndView modelAndView = new ModelAndView("userstable");
        modelAndView.addObject("users", personService.getPersonPaginated(pageIndex));
        modelAndView.addObject("prevPageVisible", pageIndex > 1);
        modelAndView.addObject("nextPageVisible", nextPageVisible);
        modelAndView.addObject("prevPage", pageIndex - 1);
        modelAndView.addObject("nextPage", pageIndex + 1);

        return modelAndView;
    }

    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public ModelAndView remove(@RequestParam("id") Integer userId) {
        if(loginHolder.getCurrentUserId() == 0) {
            return new ModelAndView("redirect:/index.html");
        }
        personRepository.deleteById(userId);

        return this.showAllUsers(1);
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam(value = "id", required = false) Integer userId) {
        if(loginHolder.getCurrentUserId() == 0) {
            return new ModelAndView("redirect:/index.html");
        }
        Person p;
        if (userId == null) {
            p = new Person();
        } else {
            p = personRepository.findById(userId).get();
        }
        ModelAndView modelAndView = new ModelAndView("userview");

        modelAndView.addObject("p", p);

        return modelAndView;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView save(Person p) {
        if(loginHolder.getCurrentUserId() == 0) {
            return new ModelAndView("redirect:/index.html");
        }
        personRepository.save(p);

        return showAllUsers(1);
    }

}
