package com.example.spring_web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class C {

    @Autowired
    A a;


    public void change() {
        a.id = a.id +1;
    }
}
