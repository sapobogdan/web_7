package com.example.spring_web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {

    @Autowired
    AdminRepository adminRepository;

    @Autowired
    LoginHolder loginHolder;

    @RequestMapping("/login")
    public ModelAndView login(@RequestParam("username") String username, @RequestParam("password") String password) {
        Admin admin = adminRepository.findByUsername(username);
        if(admin == null) {
            return new ModelAndView("redirect:/index.html");
        }
        if(!admin.getPassword().equals(password)) {
            return new ModelAndView("redirect:/index.html");
        }

        loginHolder.setCurrentUserId(admin.getId());


        return new ModelAndView("redirect:/users");

    }
}
