package com.example.spring_web;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository  extends CrudRepository<Person, Integer> {

    public Person findByAge(int age);

    @Query(value = "select * from person where age > 18", nativeQuery = true)
    public List<Person> myExtraHardQuery();

    @Query(value = " SELECT * FROM spring_awesome.person limit ?1, ?2", nativeQuery = true)
    public List<Person> getPersonsPaginated(int fromIndex, int size);
}
