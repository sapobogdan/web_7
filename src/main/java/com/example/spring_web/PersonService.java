package com.example.spring_web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public List<Person> getPersonPaginated(Integer currentPageIndex) {
        int size = 5;
        Integer fromIndex = (currentPageIndex - 1) * size;

        return personRepository.getPersonsPaginated(fromIndex, size);
    }
}
